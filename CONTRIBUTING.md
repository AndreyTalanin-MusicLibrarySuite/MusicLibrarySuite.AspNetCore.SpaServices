# Contributing Guidelines

Please read this document to the end before contributing.

## Branching model

The Gitflow workflow design is used to keep the branch structure clear and easy to maintain.

Atlassian has a thorough [tutorial](https://www.atlassian.com/git/tutorials) that will help beginners learn more about Git, including the Gitflow workflow.

In the repository, there are two long-running branches: the `main` branch for stable production versions and the `development` branch for everyday development. Both of them are protected branches.

However, there are also short-living branches of the following types:

- **Feature branches**  
  These are the branches where user stories should be implemented.  
  Created from a commit on: `development`  
  Merged into: `development`  
  Prefix: `feature/*`

- **Release branches**  
  When all milestones of an incoming release are reached and required features are implemented, a release branch should be created.  
  Created from a commit on: `development`  
  Merged into: `main`, `development`  
  Prefix: `release/*`

- **Hotfix branches**  
  If a critical bug is discovered on the `main` branch, it should be fixed immediately in a hotfix branch.  
  Created from a commit on: `main`  
  Merged into: `main`, `development`  
  Prefix: `hotfix/*`

Please note that branches are merged only via merge commits, not using the fast-forward merge or rebase.

Do not squash commits as it makes understanding the history harder, but also avoid making series of commits with non-descriptive messages like "Add multiple files". If many small changes are committed at once, use a short title grouping these changes, but provide details in the following lines.

## How to contribute

### Submitting changes

If you have some features or fixes you want to share with us, you should create a separate pull request for each feature. We will discuss your changes and merge them when they are ready.

### Names of pull requests and issues

To make it easier to maintain issues and pull requests, please name them in the following style:

`[Category / Subcategory] Description`

Examples:

> [Solution] Add the 'main.yml' GitHub Actions workflow definition file

> [CMS / Catalog Node] Increase the CatalogNode.Description field's max length to 4096 characters

Also please use the same convention for the commit messages when merging branches and pull requests:

> [CMS / Catalog] Add a warning on an attempt to close the tab with unsaved changes

If you have a problem with picking an appropriate category for the issue/pull request, the namespace of the type associated with the problem might be the right solution.

## Code style

We are mostly following conventions, guidelines and patterns provided by [Microsoft Learn](https://learn.microsoft.com/en-us/):

- [C# Identifier Naming Rules and Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/identifier-names)
- [Common C# Code Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)

You can also check articles from [Framework Design Guidelines](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/) covering advanced .NET development practices:

- [Naming Guidelines](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/naming-guidelines)
- [Type Design Guidelines](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/type)
- [Member Design Guidelines](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/member)
- [Designing for Extensibility](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/designing-for-extensibility)
- [Design Guidelines for Exceptions](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/exceptions)
- [Usage Guidelines](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/usage-guidelines)
- [Common Design Patterns](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/common-design-patterns)

There is also a `.editorconfig` file in the root directory of the repository so your text editor can be configured to follow our code style automatically.

## Setting up a new repository

This check-list covers creating a new repository on [GitLab.com](https://gitlab.com/AndreyTalanin-MusicLibrarySuite).

Perform the following steps:

1. Create an empty repository on [GitLab.com](https://gitlab.com/AndreyTalanin-MusicLibrarySuite).
2. Create an empty repository on [GitHub.com](https://github.com/AndreyTalanin-MusicLibrarySuite) for mirroring.
3. Create an empty local repository, add flies like `README.md`, `LICENSE.md`, `CONTRIBUTING.md`, make the initial commit.
4. Add both remote repositories to the list of remotes with GitLab repository named `origin` and GitHub mirror named `github-mirror`.
5. Push the `main` branch containing the initial commit to both remote repositories.
6. Push the `development` branch pointing to the same commit to both remote repositories.
7. Open and change settings of the GitLab repository. See the section below for the list of settings.
8. Open and change settings of the GitHub repository. See the section below for the list of settings.
9. Continue with setting up the repository project-wise.

### Settings to change on GitLab.com

- **[Repository -> Branch defaults]** Change the `Default branch` setting from `main` to `development`.
- **[Repository -> Protected branches]** Make the `main` branch protected.  
  Allow merge for maintainers, push for maintainers. Enable force push.
- **[Repository -> Protected branches]** Make the `development` branch protected.  
  Allow merge for developers + maintainers, push for maintainers. Enable force push.
- **[Repository -> Protected branches]** Make the `release/*` branches protected.  
  Allow merge for maintainers, push for maintainers. Enable force push.
- **[Repository -> Protected branches]** Make the `hotfix/*` branches protected.  
  Allow merge for maintainers, push for maintainers. Enable force push.
- **[Repository -> Protected tags]** Make all `*` tags protected.  
  Allow creation for maintainers.

- **[Merge requests]** Set the `Merge method` setting to `Merge commit`.
- **[Merge requests]** Set the `Squash commits when merging` setting to `Do not allow`.
- **[Merge requests]** Set the `Merge checks - Pipelines must succeed` setting to `true`.
- **[Merge requests]** Set the `Merge checks - Skipped pipelines are considered successful` setting to `true`.
- **[Merge requests]** Set the `Merge checks - All threads must be resolved` setting to `true`.

- **[CI/CD -> General pipelines]** Set the `CI/CD configuration file` setting to `.gitlab/.gitlab-ci.yml`.
- **[CI/CD -> Variables]** Add the `GITHUB_REPOSITORY_URL` variable containing the SSH-formatted GitHub repository URL.  
  (Optional, not required if `GITHUB_REPOSITORY_URL` is specified in the `.gitlab/.gitlab-ci.yml` file.)
- **[CI/CD -> Variables]** Check that the `GITHUB_REPOSITORY_SSH_PRIVATE_KEY` protected file variable is inherited from the group settings.
- **[CI/CD -> Variables]** Add the `GITLAB_REPOSITORY_URL` variable containing the SSH-formatted GitLab repository URL.  
  (Optional, not required if `GITLAB_REPOSITORY_URL` is specified in the `.gitlab/.gitlab-ci.yml` file.)
- **[CI/CD -> Variables]** Check that the `GITLAB_REPOSITORY_SSH_PRIVATE_KEY` protected file variable is inherited from the group settings.

### Settings to change on GitHub.com

- **[General]** Change the `Default branch` setting from `main` to `development`.
